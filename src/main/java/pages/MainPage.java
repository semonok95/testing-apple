package pages;

public class MainPage extends BasePage {

    @Override
    public String getUrl() {
        return "https://www.apple.com/";
    }

}
