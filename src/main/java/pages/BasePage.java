package pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public abstract class BasePage {

    protected abstract String getUrl();

    @Getter
    private final SelenideElement pageTitle = $("title");

    @Getter
    private final SelenideElement settingsMenu = $(byXpath("//li[@id='ac-gn-bag']/div/a"));

    @Getter
    private final SelenideElement signIn = $(byXpath("//a[@class='ac-gn-bagview-nav-link ac-gn-bagview-nav-link-signIn']"));

    @Getter
    private final SelenideElement signOut = $(byXpath("//a[@class='ac-gn-bagview-nav-link ac-gn-bagview-nav-link-signOut']"));

    @Getter
    private final SelenideElement account = $(byXpath("//a[@class='ac-gn-bagview-nav-link ac-gn-bagview-nav-link-account']"));

    public String getSignedInUserName() {
        if (signOut.exists()) {
            return signOut.getText().substring(9);
        } else return "";
    }

    public void openSettingMenu() throws InterruptedException {
        getSettingsMenu().click();
        Thread.sleep(1000);
        if (!getAccount().isDisplayed()) {
            getSettingsMenu().click();
        }
    }

}