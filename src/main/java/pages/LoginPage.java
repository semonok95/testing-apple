package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage extends BasePage{

    MainPage mainPage = new MainPage();


    @Override
    protected String getUrl() {
        return null;
    }

    @Getter
    private final SelenideElement signInHeader = $(byXpath("//h1[@class='as-l-container rs-sign-in-header']"));

    @Getter
    private final SelenideElement loginField = $(byXpath("//input[@id='signIn.customerLogin.appleId']"));

    @Getter
    private final SelenideElement loginError = $(byXpath("//div[@id='signIn.customerLogin.appleId_error']"));

    @Getter
    private final SelenideElement passwordField = $(byXpath("//input[@id='signIn.customerLogin.password']"));

    @Getter
    private final SelenideElement passwordError = $(byXpath("//div[@id='signIn.customerLogin.password_error']"));

    @Getter
    private final SelenideElement confirmButton = $(byXpath("//button[@id='signin-submit-button']"));

    @Getter
    private final SelenideElement mainError = $(byXpath("//div[@class='form-alert is-error']"));

    public void openLoginPage() throws InterruptedException {
        open(mainPage.getUrl());
        mainPage.openSettingMenu();
        mainPage.getSignIn().click();
        signInHeader.shouldBe(Condition.visible);
    }

}
