Feature: Sign In tests

  @signIn
  Scenario: Correct Sign In
    Given Open login page
    When Enter Apple ID "testsovich@rambler.ru"
    When Enter password "Kapusta!234"
    When Click on Sign In button
    Then "Test" user logged in

  Scenario Template: Wrong AppleID/Password

    Given Open login page
    When Enter Apple ID "<appleId>"
    When Enter password "<password>"
    When Click on Sign In button
    Then Apple ID error is "<apple Id Error>"
    Then Password error is "<password Error>"
    Then Main error is "<main Error>"

    Examples:
      | appleId  | password | apple Id Error       | password Error       | main Error                                                                                                           |
      |          |          | Apple ID is missing. | Password is missing. |                                                                                                                      |
      |          | 123456   | Apple ID is missing. |                      |                                                                                                                      |
      | semonok  |          |                      | Password is missing. |                                                                                                                      |
      | semonok1 | 123456   |                      |                      | Your account information was entered incorrectly.                                                                    |
      | asdf     | 123456   |                      |                      | This Apple ID has been locked for security reasons. Visit iForgot to reset your account (https://iforgot.apple.com). |
