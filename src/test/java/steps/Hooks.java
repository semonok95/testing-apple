package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.After;
import pages.MainPage;

public class Hooks {

    MainPage mainPage = new MainPage();

    @After("@signIn")
    public void signOut() throws InterruptedException {
        mainPage.openSettingMenu();
        mainPage.getSignOut().click();
        mainPage.openSettingMenu();
        mainPage.getSignIn().shouldBe(Condition.visible);
    }

    @After
    public void clearData() {
        Selenide.closeWebDriver();
    }

}
