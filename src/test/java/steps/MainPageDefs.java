package steps;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import pages.MainPage;

import static com.codeborne.selenide.Condition.attribute;

public class MainPageDefs {

    MainPage mainPage = new MainPage();

    @Then("{string} user logged in")
    public void userLoggedIn(String expectedUser) throws InterruptedException {
        mainPage.getPageTitle().shouldHave(attribute("text", "Apple"));
        mainPage.openSettingMenu();
        mainPage.getSignOut().shouldBe(Condition.visible);
        String actualUser = mainPage.getSignedInUserName();
        Assertions.assertEquals(expectedUser, actualUser);
    }
}
