package steps;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import pages.LoginPage;

public class LoginPageDefs {

    LoginPage loginPage = new LoginPage();

    private void errorMessageAssertion(String expectedMessage, SelenideElement errorField) {
        if (!expectedMessage.equals("")) {
            String actualMessage = errorField.getText();
            Assertions.assertEquals(expectedMessage, actualMessage);
        }
    }

    @Given("Open login page")
    public void openLoginPage() throws InterruptedException {
        loginPage.openLoginPage();
    }

    @When("Enter Apple ID {string}")
    public void enterEmail(String login) {
        loginPage.getLoginField().setValue(login);
    }

    @When("Enter password {string}")
    public void enterPassword(String password) {
        loginPage.getPasswordField().setValue(password);
    }

    @When("Click on Sign In button")
    public void clickOn() {
        loginPage.getConfirmButton().click();
    }

    @Then("Apple ID error is {string}")
    public void appleIDErrorIs(String expectedError) {
        errorMessageAssertion(expectedError, loginPage.getLoginError());
    }

    @Then("Password error is {string}")
    public void passwordErrorIs(String expectedError) {
        errorMessageAssertion(expectedError, loginPage.getPasswordError());
    }

    @Then("Main error is {string}")
    public void mainErrorIs(String expectedError) {
        errorMessageAssertion(expectedError, loginPage.getMainError());
    }
}
